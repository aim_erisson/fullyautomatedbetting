import pandas as pd
import requests
import json
from datetime import date
from datetime import timedelta

today = date.today()
url = "https://sportapi.begmedia.com/api/v2/events?sortBy=1&sportIds=2&isLive=false&FetchLiveTopMarkets=false"
payload = {"Host": "sportapi.begmedia.com","User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:80.0) Gecko/20100101 Firefox/80.0","Accept": "application/json, text/plain, */*","Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3","Accept-Encoding": "gzip, deflate, br","Range": "Item=0-1000","X-CLIENT": '{"Context":"eyJ0eXAiOiJKV1QiLCJhbGciOiJHbG9iYWwuTW9iaWxlLkFwaS5BdXRoLkFwaS5TaGEyNTZBbGdvcml0aG0ifQ.IntcIkxlZ2lzbGF0aW9uXCI6XCJGclwiLFwiU2l0ZVwiOlwiRnJGclwiLFwiTGFuZ3VhZ2VcIjpcIkZyXCIsXCJDaGFubmVsSWRcIjpcIkJldGNsaWNGclwiLFwiVW5pdmVyc2VcIjpcIlNwb3J0c1wiLFwiTm90QmVmb3JlXCI6XCIyMDIwLTA5LTA2VDEwOjI5OjM0LjU3NTY0NjFaXCIsXCJFeHBpcmF0aW9uVGltZVwiOlwiMjAyMC0wOS0wNlQxMDozNDozNC41NzU2NDYxWlwifSI.Bzlz2L3eXR8VSNN4viKlzplYM8x6nKlGPe7qb35e0YA","Auth":"eyJ0eXAiOiJKV1QiLCJhbGciOiJHbG9iYWwuTW9iaWxlLkFwaS5BdXRoLkFwaS5TaGEyNTZBbGdvcml0aG0ifQ.IntcIklwXCI6XCI4Ni4yMDIuMTI0LjIzNVwiLFwiVXNlcklkXCI6LTEsXCJTZXNzaW9uXCI6bnVsbCxcIkNvdW50cnlDb2RlXCI6bnVsbCxcIkxhbmd1YWdlQ29kZVwiOm51bGwsXCJDdXJyZW5jeUNvZGVcIjpudWxsLFwiSXNBZG1pblwiOmZhbHNlLFwiSXNMb2dnZWRGcm9tQm9cIjpmYWxzZSxcIklzTGF1bmNoZXJcIjpmYWxzZSxcIlJlZ3VsYXRvcklkXCI6MixcIk5vdEJlZm9yZVwiOlwiMjAyMC0wOS0wNlQxMDoyOTozNC41NzU2NDYxWlwiLFwiRXhwaXJhdGlvblRpbWVcIjpcIjIwMjAtMDktMDZUMTM6MzE6MzQuNTc1NjQ2MVpcIn0i.r0twfF7xT1VZSyK3myiZs-TxZ8KmNc81QSL1q-fQiic","Device":null}',"Origin": "https://www.betclic.fr","Connection": "keep-alive","Referer": "https://www.betclic.fr/calendrier/tennis-s2i1","Upgrade-Insecure-Requests": "1","Pragma": "no-cache","Cache-Control": "no-cache"}

urlBasket = "https://sportapi.begmedia.com/api/v2/events?sortBy=1&sportIds=4&isLive=false&FetchLiveTopMarkets=false"
basketPayload = {"Host": "sportapi.begmedia.com","User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:80.0) Gecko/20100101 Firefox/80.0","Accept": "application/json, text/plain, */*","Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3","Accept-Encoding": "gzip, deflate, br","Range": "Item=0-1000","X-CLIENT": '{"Context":"eyJ0eXAiOiJKV1QiLCJhbGciOiJHbG9iYWwuTW9iaWxlLkFwaS5BdXRoLkFwaS5TaGEyNTZBbGdvcml0aG0ifQ.IntcIkxlZ2lzbGF0aW9uXCI6XCJGclwiLFwiU2l0ZVwiOlwiRnJGclwiLFwiTGFuZ3VhZ2VcIjpcIkZyXCIsXCJDaGFubmVsSWRcIjpcIkJldGNsaWNGclwiLFwiVW5pdmVyc2VcIjpcIlNwb3J0c1wiLFwiTm90QmVmb3JlXCI6XCIyMDIwLTA5LTA2VDEwOjI5OjM0LjU3NTY0NjFaXCIsXCJFeHBpcmF0aW9uVGltZVwiOlwiMjAyMC0wOS0wNlQxMDozNDozNC41NzU2NDYxWlwifSI.Bzlz2L3eXR8VSNN4viKlzplYM8x6nKlGPe7qb35e0YA","Auth":"eyJ0eXAiOiJKV1QiLCJhbGciOiJHbG9iYWwuTW9iaWxlLkFwaS5BdXRoLkFwaS5TaGEyNTZBbGdvcml0aG0ifQ.IntcIklwXCI6XCI4Ni4yMDIuMTI0LjIzNVwiLFwiVXNlcklkXCI6LTEsXCJTZXNzaW9uXCI6bnVsbCxcIkNvdW50cnlDb2RlXCI6bnVsbCxcIkxhbmd1YWdlQ29kZVwiOm51bGwsXCJDdXJyZW5jeUNvZGVcIjpudWxsLFwiSXNBZG1pblwiOmZhbHNlLFwiSXNMb2dnZWRGcm9tQm9cIjpmYWxzZSxcIklzTGF1bmNoZXJcIjpmYWxzZSxcIlJlZ3VsYXRvcklkXCI6MixcIk5vdEJlZm9yZVwiOlwiMjAyMC0wOS0wNlQxMDoyOTozNC41NzU2NDYxWlwiLFwiRXhwaXJhdGlvblRpbWVcIjpcIjIwMjAtMDktMDZUMTM6MzE6MzQuNTc1NjQ2MVpcIn0i.r0twfF7xT1VZSyK3myiZs-TxZ8KmNc81QSL1q-fQiic","Device":null}',"Origin": "https://www.betclic.fr","Connection": "keep-alive","Referer": "https://www.betclic.fr/calendrier/basket-ball-s4i1","Upgrade-Insecure-Requests": "1","Pragma": "no-cache","Cache-Control": "no-cache"}

MAXIMAL_ODDS = 1.4
MINIMAL_DIFFERENCE_BTW_ODDS = 1.7
MINIMAL_ODDS = 1.08


def getData():
    global df
    dataTennis = requests.get(url, headers=payload)
    dataBasket = requests.get(urlBasket, headers=basketPayload)
    df = pd.DataFrame.from_dict( dataBasket.json() + dataTennis.json() )
    print(df)
    print(len(df), "--------------------")
    transformData()

def transformData():

    for i in range(len(df)):
        if not df['markets'][i]:
            df.drop(i, inplace=True)
    df.reset_index(inplace=True)
    print(df)

    df['date'] = df['date'].apply(lambda a: pd.to_datetime(a).date())

    df["odds1"] = 0.01
    df["odds2"] = 0.01
    df["bestOdds"] = 0.01
    df["bestOddsID"] = 000000
    df["CompetitionName"] = "do not know"
    df["Sport"] = "do not know"
    a = 0
    for i in range(0, len(df)):
        if( today + timedelta(days=1) == df['date'][i]):
            print("")
        else:
            df.drop(i, inplace=True)
            a = a +1
    print("------------------", a, "---------------------")
    df.reset_index(inplace=True)
    for i in range(0, len(df)):

        odds1 = df['markets'][i][0]["selections"][0]['odds']
        df["odds1"][i] = odds1

        odds2 = df['markets'][i][0]["selections"][1]['odds']
        df["odds2"][i] = odds2

        df["CompetitionName"][i] = df['competition'][i]['name']
        df["Sport"][i] = df['competition'][i]['sport']['name']


        if odds1 < odds2:
            df["bestOdds"][i] = odds1
            df["bestOddsID"][i] = df['markets'][i][0]["selections"][0]['id']
        else:
            df["bestOdds"][i] = odds2
            df["bestOddsID"][i] = df['markets'][i][0]["selections"][1]['id']




    del df['markets']
    del df['contestants']
    del df['status']
    del df['openMarketCount']
    del df['hasLive']
    del df['isLive']
    del df['relativeDesktopUrl']
    del df['competition']
    del df['iafc']
    del df['index']
    del df['isOpen']
    del df['hasLiveStreaming']

    print(df.head())
    chooseGames()
    # df.to_excel('ParisDeDemain.xlsx')
def chooseGames():
    globalDf = pd.read_excel('gameExcel.xlsx')

    for i in range(0, len(df)):

        odds = df['bestOdds'][i]
        # and  odds > MINIMAL_ODDS
        if not (odds < MAXIMAL_ODDS  and abs(df['odds1'][i] - df['odds2'][i]) > MINIMAL_DIFFERENCE_BTW_ODDS and odds > MINIMAL_ODDS ):
            df.drop(i, inplace=True)


    print('aaaa')
    df.reset_index(inplace=True)
    print(df['bestOdds'])
    print(len(df),"--------------")

    df.to_json('game.json')
