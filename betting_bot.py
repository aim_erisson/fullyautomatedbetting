import pandas as pd
from selenium.webdriver.common.keys import Keys
from seleniumrequests import Firefox
from time import sleep
import json

username = "xxxxx"
password = "xxxx#"
BirthDate_Day = "xxxxx"
BirthDate_Month = "xxxx"
BirthDate_Year = "xxxxx"
BET_SOME = "xxx"
# This function is use to bet on the betclic webpage

def bot():

    df = pd.read_json('game.json')
    print(df.head())
    with open('listfile.txt', 'r') as f:
        groupList = json.loads(f.read())
    # starts the browser and loads the webpage
    driver = Firefox()
    driver.implicitly_wait(5)
    driver.get('https://www.betclic.fr/calendrier/tennis-s2i1')

    # Logs in the user
    login = driver.find_element_by_id("login-username")
    login.send_keys(username)
    login = driver.find_element_by_id("login-password")
    login.send_keys(password)
    login.send_keys(Keys.RETURN)

    sleep(1)

    date = driver.find_element_by_id("CustBirthDate_Day")
    date.send_keys(BirthDate_Day)
    date = driver.find_element_by_id("CustBirthDate_Month")
    date.send_keys(BirthDate_Month)
    date = driver.find_element_by_id("CustBirthDate_Year")
    date.send_keys(BirthDate_Year)
    date.send_keys(Keys.RETURN)
    print(groupList)
    global argent
    argentSurCompte = driver.find_element_by_class_name("bonus")
    argentSurCompte = argentSurCompte.text.replace('€', '')
    argentSurCompte = argentSurCompte.replace(' ', '')
    argentSurCompte = float(argentSurCompte.replace(',', '.'))
    print(argentSurCompte)

    # This loop makes the combined bets and then bets on it
    for i in range(0, len(groupList)):
        cote = 1
        for o in range(0,len(groupList[i])):
            print("je mise sur l'id: ", df["bestOddsID"][groupList[i][o]])
            cote = cote * df["bestOdds"][groupList[i][o]]
            driver.request('POST', 'https://www.betclic.fr/bet/addcart', data={"id":df["bestOddsID"][groupList[i][o]]})
            sleep(1)

        BET_SOME = KellyAlgo (cote , argentSurCompte )

        argentSurCompte = argentSurCompte - BET_SOME
        print("OK all good je parie : ", BET_SOME)

        driver.refresh()
        placeBetValue = driver.find_element_by_id("MultipleStake")
        placeBetValue.send_keys(str(BET_SOME))

        sleep(1)
        placeBetValue.send_keys(Keys.RETURN)
        sleep(1)
        driver.find_element_by_id("close-confirm").click()
        sleep(1)
        print("je parie")
        print('--------------')


def KellyAlgo (odds , argent ):
    Pwin = round(1/odds, 2)
    Ploss = 1 - Pwin

    pourcentage = (Pwin * odds - Ploss)/odds
    argentAparier = round(argent * (1/3 * pourcentage),2)
    return(argentAparier)
