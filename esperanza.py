from get_and_transform_data import getData
from betting_bot import bot
from make_groups import createGroups
from make_groups import oldList
import pandas as pd


def do_all():

    getData()
    createGroups()
    bot()

def getList():
    groupList = []
    with open('listfile.txt', 'r') as filehandle:
        for line in filehandle:
            # remove linebreak which is the last character of the string
            trio = line[:-1]

            # add item to the list
            groupList.append(list(trio))
    return groupList
    
do_all()
