import pandas as pd
import random
import json

df = pd.read_json('game.json')
NUMBER_OF_COMPARISION = 10000
oldList = []
def createGroups():
    chooseBestGroups()

def makeGroups():
    groupList = []
    numberList = [*range(0,len(df), 1)]
    random.shuffle(numberList)
    i = 0

    while i < ((len(numberList)//3)*3):


        groupList.append([numberList[i],numberList[i+1],numberList[i+2]])
        i = i + 3

    return groupList

def chooseBestGroups():



    global oldList
    oldList = makeGroups()

    z = 0
    while z < NUMBER_OF_COMPARISION:
        newList = makeGroups()
        ecart1 = 0
        ecart2 = 0
        for i in range(0, len(oldList)):
            cote1 = 1
            cote2 = 1
            for o in range(0,3):
                cote1 = cote1 * df['bestOdds'][oldList[i][o]]
                cote2 = cote2 * df['bestOdds'][newList[i][o]]
            ecart1 = ecart1 + abs(2 - cote1)
            ecart2 = ecart2 + abs(2 - cote2)

        if(ecart2 < ecart1):
            oldList = newList
            # print(ecart2)
        # else:
            # print(ecart1)

        z = z + 1

    print(oldList)
    print(ecart1, ' ------- ', ecart2)


    for i in range(0, len(oldList)):
        cote = 1

        print('|combiné ', i + 1,'|')
        print('--------------')
        for o in range(0,3):
            cote = cote * df['bestOdds'][oldList[i][o]]
            print(abs(df['odds1'][oldList[i][o]]-df['odds2'][oldList[i][o]]), df['name'][oldList[i][o]], '////------>',  df['bestOdds'][oldList[i][o]])

        print('--------------')

    print(oldList)
    with open('listfile.txt', 'w') as f:
            f.write(json.dumps(oldList))
